#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) {
    if (!addr) {
        return REGION_INVALID;
    }

    size_t actual_size = region_actual_size(query);
    size_t ext_query = size_from_capacity((block_capacity) {actual_size}).bytes;


    void *memory = map_pages(addr, ext_query, MAP_FIXED);

    bool extends = true;
    if (addr != memory) {
        extends = false;
        memory = map_pages(addr, ext_query, 0);
    }

    block_init(memory, size_from_capacity((block_capacity) {ext_query}), NULL);

    return (struct region) {
            memory,
            ext_query,
            extends,
    };
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    HEAP_START = region.addr;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
    if (block == NULL || !block_splittable(block, query)) {
        return false;
    }

    size_t first_block_size = size_from_capacity((block_capacity) {query}).bytes;
    void *next_block_ptr = (uint8_t*)block + first_block_size;
    block_size next_block_size = (block_size) {size_from_capacity(block->capacity).bytes - first_block_size};

    block_init(next_block_ptr, next_block_size, block->next);
    block_init(block, size_from_capacity((block_capacity) {query}), next_block_ptr);

    return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
    if (!block || !block->next || !mergeable(block, block->next)) {
        return false;
    }

    block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
    block->next = block->next->next;

    return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};


static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
    struct block_header *to_return = NULL;

    if (block == NULL) {
        return (struct block_search_result) {
                .type = BSR_REACHED_END_NOT_FOUND,
                .block = to_return,
        };
    }

    while (block) {
        to_return = block;
        while (try_merge_with_next(block)) {}

        if (block->is_free && block_is_big_enough(sz, block)) {
            return (struct block_search_result) {
                    .type = BSR_FOUND_GOOD_BLOCK,
                    .block = block,
            };
        }

        block = block->next;
    }

    return (struct block_search_result) {
            .type = BSR_REACHED_END_NOT_FOUND,
            .block = to_return,
    };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
    struct block_search_result result =  find_good_or_last(block, query);
    if (result.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(result.block, query);
        result.block->is_free = false;
    }

    return result;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    if (last == NULL) {
        return NULL;
    }

    struct region new_region = alloc_region(block_after(last), query);
    if (region_is_invalid(&new_region)) {
        fprintf(stderr, "unable to grow heap");
        fflush(stderr);
        return NULL;
    }

    if (last->is_free && new_region.extends) {
        last->capacity.bytes += new_region.size;
    } else {
        last->next = new_region.addr;
    }

    return new_region.addr;

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
    struct block_search_result result = try_memalloc_existing(size_max(query, BLOCK_MIN_CAPACITY), heap_start);

    if (result.type == BSR_REACHED_END_NOT_FOUND) {
        if (!grow_heap(result.block, query)) {
            return NULL;
        }

        result = try_memalloc_existing(query, heap_start);
    }

    split_if_too_big(result.block, query);
    return result.block;

}

void *_malloc(size_t query) {
    struct block_header *const addr = memalloc(query, (struct block_header *) HEAP_START);
    if (addr) return addr->contents;
    else return NULL;
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem) return;
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while (try_merge_with_next(header)) {}
}
