#ifndef ALLOCATOR_ALLOCATOR_TESTS_H
#define ALLOCATOR_ALLOCATOR_TESTS_H

#include <stdbool.h>
#include <stddef.h>

bool alloc_test_without_heap_growing(size_t query);

bool free_function_test();

bool allocate_two_regions_test();

#endif //ALLOCATOR_ALLOCATOR_TESTS_H
