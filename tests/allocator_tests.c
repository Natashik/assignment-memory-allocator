#include "../src/mem.h"
#include "allocator_tests.h"

#include <assert.h>

size_t TEST_COUNTER = 0;

static struct block_header *block_header_from_ptr(void *ptr) {
    return (struct block_header *) ((uint8_t *) ptr - offsetof(struct block_header, contents));
}

bool alloc_test_without_heap_growing(size_t query) {
    assert(query < REGION_MIN_SIZE && "This test only for query with less capacity than default");
    fprintf(stdout, "[TEST %zu]: Allocation of %zu bytes\n", ++TEST_COUNTER, query);
    void *result = _malloc(query);

    struct block_header *header = block_header_from_ptr(result);

    if (!result) {
        fprintf(stderr, "Unable to allocate memory");
        _free(result);
        return false;
    }

    if (header->is_free != false) {
        fprintf(stderr, "Header block must not be free");
        _free(result);
        return false;
    }

    if (header->next == NULL || header->next->next != NULL) {
        fprintf(stderr, "Header block next pointers are incorrect");
        _free(result);
        return false;
    }

    size_t expected_capacity = size_max(24, query);
    if (header->capacity.bytes != expected_capacity) {
        fprintf(stderr, "Header block capacity must be %zu, your is %zu", expected_capacity, header->capacity.bytes);
        _free(result);
        return false;
    }

    _free(result);

    fprintf(stdout, "[TEST %zu]: Success\n\n", TEST_COUNTER);
    return true;
}

bool free_function_test() {
    fprintf(stdout, "[TEST %zu]: Allocation of 5000 bytes, then free and again allocation\n", ++TEST_COUNTER);

    void *first = _malloc(5000);
    _free(first);
    void *second = _malloc(5000);

    if (first != second) {
        fprintf(stderr, "Pointer after free must have same addresses");
        _free(second);
        return false;
    }

    _free(second);

    fprintf(stdout, "[TEST %zu]: Success\n\n", TEST_COUNTER);
    return true;
}

bool allocate_two_regions_test() {
    fprintf(stdout, "[TEST %zu]: Allocation of 5000 bytes two times, don't fit in one region\n", ++TEST_COUNTER);

    // they don't fit in single region
    void *first = _malloc(5000);
    void *second = _malloc(5000);

    struct block_header *first_header = block_header_from_ptr(first);
    struct block_header *second_header = block_header_from_ptr(second);

    if (first_header->next->next != second_header) {
        fprintf(stderr, "first_header->next->next must equal  second_header");
        _free(first);
        _free(second);
        return false;
    }

    if (first_header->next->next->next != second_header->next) {
        fprintf(stderr, "first_header->next->next->next must equal second_header->next");
        _free(first);
        _free(second);
        return false;
    }

    _free(first);
    _free(second);

    fprintf(stdout, "[TEST %zu]: Success\n\n", TEST_COUNTER);
    return true;
}

int main() {
    if (!heap_init(REGION_MIN_SIZE)) {
        fprintf(stderr, "Unable to initialize heap");
        return false;
    }

    if (!alloc_test_without_heap_growing(10) ||
            !alloc_test_without_heap_growing(200) ||
            !alloc_test_without_heap_growing(1000) ||
            !alloc_test_without_heap_growing(3000) ||
            !free_function_test() ||
            !allocate_two_regions_test()
            ) {
        fprintf(stderr, "Test failed!");
        return -1;
    }

    fprintf(stdout, "All tests successfully passed!");
    return 0;
}
