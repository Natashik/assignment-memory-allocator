#include <printf.h>

#include "../src/mem.h"

#define TEST_STRING_SIZE_LEN 10

int main() {
    struct block_header* reg = heap_init(REGION_MIN_SIZE);
    if (!reg) {
        fprintf(stderr, "heap init error");
        return -1;
    }


    char* str = _malloc((TEST_STRING_SIZE_LEN + 1) * sizeof(char));
    for (size_t i = 0; i < TEST_STRING_SIZE_LEN; ++i) {
        str[i] = i + '0';
    }

    str[TEST_STRING_SIZE_LEN] = '\0';
    printf("%s", str);

    _free(str);
}
